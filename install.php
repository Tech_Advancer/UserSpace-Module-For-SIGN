<?php
//////////////////////////
//
//  install.php
//  Installs the UserSpace Module.
//  Included by the SIGN admin panel.
//
//////////////////////////


// Security check below:

if($user->getNumber() == -1){ die(lang::genericError); } // IF the user isn't logged in
if(!$user->getAdmin()){ die(lang::genericError); } // IF the current user isn't admin
if(!$user->getReauth('adminPanel')){ die(lang::genericError); } // IF the user hasn't reauthenticated
if(!$user->updateAdminSessionTime()){ die(lang::genericError); } // IF the admin's session time is out


if((isset($_POST['userSpaceName'])) && (isset($moduleLocation)) && (isset($_POST['adminEmail'])) && (isset($_POST['language'])) && (isset($_POST['submit']))){ // IF the form has been submitted
	$link = $sql->getLink();

	$moduleName = $sql->db_safe($_POST['userSpaceName'], $link);
	$adminEmail = $_POST['adminEmail'];
	$language = $_POST['language'];

	if(isset($_POST['canRead'])){ $defaultRead = 1; }else{ $defaultRead = 0; }
	if(isset($_POST['canPost'])){ $defaultPost = 1; }else{ $defaultPost = 0; }
	if(isset($_POST['visible'])){ $visible = 1; }else{ $visible = 0; }

	$moduleId = $sql->addModule( $moduleName, $moduleLocation );

	if(is_null($moduleId)){ die(lang::genericError); }

	$module = $sql->getModule($moduleId);


	// Setup Query Array for our loop below
	$query = [];


	// DROP ALL IF EXISTS:
	$query[] = 'DROP TABLE IF EXISTS UserSpace_' . $moduleId . '_Settings_' . $module['postfix'];
	$query[] = 'DROP TABLE IF EXISTS UserSpace_' . $moduleId . '_Comments_' . $module['postfix'];
	$query[] = 'DROP TABLE IF EXISTS UserSpace_' . $moduleId . '_Posts_' . $module['postfix'];
	$query[] = 'DROP TABLE IF EXISTS UserSpace_' . $moduleId . '_Pages_' . $module['postfix'];
	$query[] = 'DROP TABLE IF EXISTS UserSpace_' . $moduleId . '_Users_' . $module['postfix'];



	$query[] = 'CREATE TABLE UserSpace_' . $moduleId . '_Users_' . $module['postfix'] . '(userSpaceId bigint(20) NOT NULL AUTO_INCREMENT, userId bigint(20) NOT NULL, userCanRead int(1), userCanPost int(1), userLanguage varchar(255), pageId bigint(20), PRIMARY KEY(userSpaceId))';

	$query[] = 'CREATE TABLE UserSpace_' . $moduleId . '_Pages_' . $module['postfix'] . '(pageId bigint(20) NOT NULL AUTO_INCREMENT, userId bigint(20) NOT NULL, pageName varchar(255) NOT NULL, pageType int(4) NOT NULL, pageContent longtext, pagePriority bigint(20), PRIMARY KEY(pageId))';

	$query[] = 'CREATE TABLE UserSpace_' . $moduleId . '_Posts_' . $module['postfix'] . '(postId bigint(20) NOT NULL AUTO_INCREMENT, pageId bigint(20) NOT NULL, postContent longtext, postLink varchar(255), PRIMARY KEY(postId), FOREIGN KEY(pageId) REFERENCES UserSpace_' . $moduleId . '_Pages_' . $module['postfix'] . '(pageId))';

	$query[] = 'CREATE TABLE UserSpace_' . $moduleId . '_Comments_' . $module['postfix'] . '(commentId bigint(20) NOT NULL AUTO_INCREMENT, postId bigint(20) NOT NULL, userId bigint(20) NOT NULL, commentContent longtext, PRIMARY KEY(commentId), FOREIGN KEY(postId) REFERENCES UserSpace_' . $moduleId . '_Posts_' . $module['postfix'] . '(postId))';

	$query[] = 'CREATE TABLE UserSpace_' . $moduleId . '_Settings_' . $module['postfix'] . '(settingId bigint(20) NOT NULL AUTO_INCREMENT, settingName text NOT NULL, settingNumber bigint(20), settingString longtext, PRIMARY KEY(settingId))';


	// Loop though our queries and execute them:
        foreach($query as $mysqlQuery){
	    if(!$result = mysqli_query($link, $mysqlQuery)){
		die('<p>Error!</p><p>' . mysqli_error($link) . '</p><p>' . $mysqlQuery . '</p>');
	    }
	    unset($result);
	}
	unset($query); unset($mysqlQuery);

	// Go back and add our missing foreign key. We couldn't add it before since SIGN doesn't give out its postfix to modules.
	if(!$sql->addForeignKey('UserSpace_' . $moduleId . '_Users_' . $module['postfix'], 'userId')){ die(lang::genericError . 'a'); }
        if(!$sql->addForeignKey('UserSpace_' . $moduleId . '_Pages_' . $module['postfix'], 'userId')){ die(lang::genericError); }
        if(!$sql->addForeignKey('UserSpace_' . $moduleId . '_Comments_' . $module['postfix'], 'userId')){ die(lang::genericError); }

        $users = $sql->getUserList();

	if(is_null($users)){ die(lang::genericError); }

        // Add users to our UserSpace Users table. It holds settings specific to this installation for users.
        foreach($users as $item){
	    if($stmt = $link->prepare('INSERT INTO UserSpace_' . $moduleId . '_Users_' . $module['postfix'] . '(userId, userCanRead, userCanPost) VALUES(?,?,?)')){

		if($item['userSignAdmin'] != 1){
		    $stmt->bind_param('iii', $item['userId'], $defaultRead, $defaultPost);
		}else{
		    $read = 1;
		    $post = 1;
		    $stmt->bind_param('iii', $item['userId'], $read, $post);
		}
                $stmt->execute();
                $stmt->close();
		unset($stmt);
            }
        }


        $query = 'INSERT INTO UserSpace_' . $moduleId . '_Settings_' . $module['postfix'] . '(settingName,settingNumber,settingString) VALUES (?,?,?)';
        $stmt = mysqli_stmt_init($link);
        if(mysqli_stmt_prepare($stmt, $query)){

	    $name = 'userSpaceName';
            $number = null;
            $string = $moduleName;

            mysqli_stmt_bind_param($stmt, "sis", $name, $number, $string);
            mysqli_stmt_execute($stmt);

            $name = 'defaultUserReadSetting';
            $number = $defaultRead;
            $string = null;

            mysqli_stmt_bind_param($stmt, "sis", $name, $number, $string);
            mysqli_stmt_execute($stmt);

            $name = 'defaultUserPostSetting';
            $number = $defaultPost;
            $string = null;

            mysqli_stmt_bind_param($stmt, "sis", $name, $number, $string);
            mysqli_stmt_execute($stmt);

            $name = 'guestReadSetting';
            $number = $visible;
            $string = null;

            mysqli_stmt_bind_param($stmt, "sis", $name, $number, $string);
            mysqli_stmt_execute($stmt);

  	    $name = 'defaultLanguage';
            $number = null;
            $string = $language;

            mysqli_stmt_bind_param($stmt, "sis", $name, $number, $string);
            mysqli_stmt_execute($stmt);

            $name = 'joinSetting';
            $number = $visible;
            $string = null;

            mysqli_stmt_bind_param($stmt, "sis", $name, $number, $string);
            mysqli_stmt_execute($stmt);

 	    $name = 'adminEmail';
            $number = null;
            $string = $adminEmail;

            mysqli_stmt_bind_param($stmt, "sis", $name, $number, $string);
            mysqli_stmt_execute($stmt);

		$stmt->close();
		unset($stmt);


	}else{
	    die(lang::genericError);
	}

        unset($link);
	$sql->setResetFlag();
?>
<div id="pageTitle">
<h1>Finished Installing the UserSpace Module!</h1>
</div>


<div id="content">

<?php include( $root . '/signPages/html/adminLinks.html'); ?>

<div class="post"> <!-- post -->
<?php echo lang::adminAddModule_done; ?>
</div> <!-- END post -->

</div> <!-- END content -->

<?php

}else{ // ELSE IF the form has been submitted yet:



	$languageFiles = scandir( $root . $moduleLocation . '/lang/'); // Get all of the language files
	$languageOptions = '';

	foreach($languageFiles as $langOP){ // Go through each
	     	if( strcmp(substr(strrchr($langOP,'.'),1), 'php') == 0 ){ // Check if they are php files
                        $languageOptions .= '<option value="' . $langOP . '">' . substr($langOP,0,strlen($langOP)-4) . '</option>' . "\n"; // insert into string
		}
	}

?>


<div id="pageTitle">
<h1>Installing the UserSpace Module</h1>
</div>


<div id="content">

<?php include( $root . '/signPages/html/adminLinks.html'); ?>

<div class="post"> <!-- post -->

<form action="./index.php?a=4" method="POST" autocomplete="off">

<label>Module Name: <input type="text" name="userSpaceName" value="UserSpace"></label><br><br>

<label>Admin Email: <input type="text" name="adminEmail"></label>
<p>&nbsp;&nbsp;&nbsp;&nbsp;This is the support email users can contact to get help when they need it. If you leave
 it blank, several error messages that a user might see won't have a contact email address.</p>

<input type="hidden" name="moduleLocation" value="<?php echo $moduleLocation; ?>">

<label><input type="checkbox" name="canRead" value="1" checked> Allow Users to Read Pages/Posts by Default?</label>
<p>&nbsp;&nbsp;&nbsp;&nbsp;Check above to allow logged in users to read posts made on this UserSpace.</p>

<label><input type="checkbox" name="canPost" value="1" checked> Allow Users to Post Pages/Posts by Default?</label>
<p>&nbsp;&nbsp;&nbsp;&nbsp;Check above to allow logged in users to make new posts and pages on this UserSpace.</p>

<label><input type="checkbox" name="visible" value="1"> Make visible to guests?</label>
<p>&nbsp;&nbsp;&nbsp;&nbsp;Check above if you want users who aren't logged in to be able to read pages and posts.</p>

<label>Language: <select name="language">
<?php echo $languageOptions; ?>
</select><br><br>

<br><br>
<input type="submit" value="Install" name="submit">
</form>


</div> <!-- END post -->
</div> <!-- END content -->



<?php
} // END ELSE IF the form has been submitted yet
?>
