<?php
////////////////////////////
//
//  moduleJoin.php
//
//  Included by SIGN's sql class.
//  Used to set db values for this module
//    when joining from another module.
//
//  All variables set in SIGN's addUser
//    and getModules are available here,
//    but shouldn't be changed!!
//
///////////////////////////

// All new variables named sub(whatever) since this is run under the main installer.
// All variable from SIGN's sql addUser and modules are available here except for $password:
// $userName, $email, $uid, $activationCode (could be null), $admin
// $module['id'], $module['name'], $module['location'], $module['postfix'] (only this module's postfix is available)


// We have to break the normal object oriented code and treat this like an additional script since
// this could be loaded from another installer and not have access to all of the same classes.



// Get default read settings:

if( $stmt = $this->dbLink->prepare('SELECT settingNumber FROM UserSpace_' . $module['id'] . '_Settings_' . $module['postfix'] . ' WHERE settingName=? LIMIT 1')){
	$name = 'defaultUserReadSetting';
   	$stmt->bind_param('s', $name);

     	if($stmt->execute()){
          	$stmt->store_result();
              	$stmt->bind_result($defaultUserReadSetting);
               	$stmt->fetch();

              	if($stmt->num_rows != 1){
                  	die(lang::genericError);
             	}
    	}else{
          	die(lang::genericError);
    	}
}else{
 	die(lang::genericError);
}

$stmt->close();
unset($stmt);



// Get default post setting:

if( $stmt = $this->dbLink->prepare('SELECT settingNumber FROM UserSpace_' . $module['id'] . '_Settings_' . $module['postfix'] . ' WHERE settingName=? LIMIT 1')){
        $name = 'defaultUserPostSetting';
        $stmt->bind_param('s', $name);

        if($stmt->execute()){
                $stmt->store_result();
                $stmt->bind_result($defaultUserPostSetting);
                $stmt->fetch();

                if($stmt->num_rows != 1){
                        die(lang::genericError);
                }
        }else{
                die(lang::genericError);
        }
}else{
        die(lang::genericError);
}

$stmt->close();
unset($stmt);




if($stmt = $this->dbLink->prepare('INSERT INTO UserSpace_' . $module['id'] . '_Users_' . $module['postfix'] . '(userId, userCanRead, userCanPost) VALUES (?,?,?)')){

	$stmt->bind_param("iii", $userId, $defaultUserReadSetting, $defaultUserPostSetting);

       	if(!$stmt->execute()){
              	die(lang::genericError);
      	}

}else{
	die(lang::genericError);
}

$stmt->close();
unset($stmt);



?>
