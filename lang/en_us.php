<?php
if(isset($includeCheck)){
// IF included from another source

abstract class mlang
{
	// Module messages:
	const welcome = 'Welcome to UserSpace!';

	// Login Messages:
	const login_default = 'Welcome to our site!';

	const login_alreadyLoggedIn1 = 'You are already logged in!<br><a href="./index.php?m=';
	const login_alreadyLoggedIn2 = '">Return</a>';

	const login_good1 = 'Login Successful!<br><a href="./index.php?m=';
	const login_good2 = '">Continue</a>';

	const logout1 = 'You have been logged out.<br><a href="./index.php?m=';
	const logout2 = '">Return</a>';

	const login_bad = 'Your username or password is incorrect.';

	const ban1 = 'You have been banned until ';
	const ban2 = '. Reason given:<br>';

	const activation = 'You can\'t login because your account hasn\'t been activated yet! Check your email for the activation link.';

	const invalidMessage = 'Invalid Message.';

	const login_maxAttempts1 = 'You have used all of your login attempts. Please try again after ';
	const login_maxAttempts2 = ' minutes.';


	// Home:
	const home_title = 'Home';

	// Join Messages:
	const join_userNameInUse = 'That username has already been taken. Please use another.';
        const join_emailInUse = 'That email address is already being used by another account.';
	const join_emailValid = 'That email address doesn\'t seem valid.';
	const join_userNameLength = 'Your username MUST be between 3 and 25 characters long!';
	const join_passwordLength = 'Your password MUST be between 8 and 70 characters long!';
	const join_passwordMatch = 'Your password didn\'t match! It IS case sensitive!';
	const join_success = 'Your account has been registered!<br><a href="./index.php">Continue</a>';
	const join_emailCodeError = 'Error sending activation code. Please try again later.';
	const join_accountActivationGood = 'Your account has been activated. You can now login.';
	const join_accountActivationBad = 'There seems to be a problem with your activation code. If it keeps failing, try copying and pasting it into your browser.';
	const join_emailSubject = 'Thank you for joining!';
	const join_emailBody1 = 'Thank you for joining ';
	const join_emailBody2 = '! To verify your account, please click the link below:<br>';
	const join_title = 'Register for an Account';
	const join_usernameLabel = 'Username: ';
	const join_password = 'Password: ';
	const join_reenterPassword = 'Password Again: ';
	const join_email = 'Email: ';
	const join_button = 'Join';
	const join_disabled1 = 'Sorry, the administrator has disabled joining.<br><a href="./index.php?m=';
	const join_disabled2 = '">Return</a>';
	const join_errorAddingUser1 = 'Error creating a new account. If this continues, contact the admin at:<br><a href="mailto:';
	const join_errorAddingUser2 = '">';
	const join_errorAddingUser3 = '</a>';
}

}else{ // ELSE IF included from another php source
if(headers_sent()){ die(include("404.html")); } // If headers were somehow already sent, just die to a custom 404 page
header("HTTP/1.1 404 Not Found"); // Send headers if not sent already
die(include("404.html")); // Die to custom 404 page
} // END ELSE IF included from another source

?>
