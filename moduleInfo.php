<?php
/////////////////////////////
//
//  installInfo.php
//  Gives admin's more
//  information about a
//  module before installing
//  it.
//
/////////////////////////////


$moduleName = 'UserSpace';
$moduleDescription = 'Allows users to create their own pages and make posts to them.';
$moduleWebsite = '';
$moduleVersion = '1.0.0';

?>
