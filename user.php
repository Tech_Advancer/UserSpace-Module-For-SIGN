<?php

if(isset($includeCheck)){
// IF included from another source

class mUser{

        private $session, $sql, $msql, $moduleId;

        public function __construct($sql, $msql, $session, $moduleId){
                // Sessions are attached to users, so define it here.
                $this->session = $session;
                $this->sql = $sql;
		$this->msql = $msql;
		$this->moduleId = $moduleId; // We use module id to make sure session values don't conflict with other modules
        }

	public function getName(){
                return $this->session->get('userName');
        }

        public function getNumber(){
                // Returns the user's number
                return $this->session->get('userId');
        }

	public function getData(){
		return $this->msql->getData($this->getNumber());
	}

	public function getAdminEmail(){
		// Although this seems like something that should be in sql, since this loads every page we want to put this into a session variable so that we don't spam the database.

		if($this->session->check('UserSpace' . $this->moduleId . 'AdminEmail')){  // Check if it is stored in session data
                        return $this->session->get('UserSpace' . $this->moduleId . 'AdminEmail');
                }else{
			$adminEmail = $this->msql->getSetting('adminEmail');
                       	$adminEmail = $adminEmail['string'];

                        $this->session->set('UserSpace' . $this->moduleId . 'AdminEmail', $adminEmail);
                        return $adminEmail;
                }
	}

	public function checkSetup(){
		// checks if this is the first time the user is loading this module (per session)
		// true = yes
		// false = no
		// We use module id to make sure session values don't conflict with other modules

		if((!$this->session->check('UserSpace' . $this->moduleId . 'LoadCheck')) || (!$this->session->check('UserSpace' . $this->moduleId . 'Language')) || (!$this->session->check('timezone'))){
			// Reset the user's cache
			$this->resetCache();

			// Reset the basic session data for this module
			$language = $this->getLanguage();
			$this->session->set('UserSpace' . $this->moduleId . 'LoadCheck', 1);
			return true;
		}else{
			return false;
		}
	}

	public function getLanguage(){
                // Gets the preferred language from the user. If it isn't set it gets the default language from the database.

                if($this->session->check('UserSpace' . $this->moduleId . 'Language')){  // Check if it is stored in session data
                        return $this->session->get('UserSpace' . $this->moduleId . 'Language');
                }else{

			if($this->getNumber() > -1){
				$userData = $this->msql->getUserData($this->getNumber());

	                        if((isset($userData['userLanguage'])) && (!is_null($userData['userLanguage'])) && (strlen($userData['userLanguage']) > 2)){ // IF user has a preference for a language
	                                $language = $userData['userLanguage'];
	                        }else{
	                                // if not, just get the default language
	                                $language = $this->msql->getSetting('defaultLanguage');
	                                $language = $language['string'];
	                        }

	                        unset($userData);
			}else{
				$language = $this->msql->getSetting('defaultLanguage');
                               	$language = $language['string'];
			}
	            	$this->session->set('UserSpace' . $this->moduleId . 'Language', $language);
	              	return $language;
                }
        }

	public function setLanguage($language){
                // Sets the language for a user
                // Returns true/false on success/failure

                if($this->sql->setUserLanguage($this->getNumber(), $language)){
			$this->session->set('UserSpace' . $this->moduleId . 'Language', $language);
                        return true;
                }else{
                        return false;
                }
        }

	public function setSessionLanguage($language){
		// Used on login to set the language to the user's preference

		$this->session->set('UserSpace' . $this->moduleId . 'Language', $language);
	}

	public function resetCache(){
		$this->session->remove('UserSpace' . $this->moduleId . 'Language');
	}

} // END mUser Class

}else{ // ELSE IF included from another php source
if(headers_sent()){ die(include('../404.html')); } // If headers were somehow already sent, just die to a custom 404 page
header('HTTP/1.1 404 Not Found'); // Send headers if not sent already
die(include('../404.html')); // Die to custom 404 page
} // END ELSE IF included from another source

?>
