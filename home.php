<?php
//////////////////////////////
//
//  home.php
//  Included by module.php
//  The front page.
//
//////////////////////////////

include($root . $module['location'] . $themePath . "header.html");
include($root . $module['location'] . $themePath . "home.html");
include($root . $module['location'] . $themePath . "footer.html");
?>
