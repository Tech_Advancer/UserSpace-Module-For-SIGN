<?php
//////////////////////////////
//
//  module.php
//  Included by index.php
//  This is the transition from
//  SIGN's loader to the module.
//
//////////////////////////////

// Set in db later:
$themePath = 'themes/basic1.0/';


switch($dbType){ // Swap out databases depending on given dbType
        case 1: $mdbType = '/mySQLiClass.php'; break;
        default: $mdbType = '/mySQLiClass.php'; break;
}

// Include our database class file. $dbType is from dbConfig.php
if(!@include_once($root . $module['location'] . $mdbType)){
        die(lang::missingMySQLi);
}

// Basically the same as SIGN, but with queries specific to this module:
$msql = new mSqlSwap($sql->getLink(), $root, $module['id'], $module['location'], $module['postfix']);

// Get module specific User.php
if(!include_once($root . $module['location'] . 'user.php')){
        die(lang::missingUser);
}

$muser = new mUser($sql->getLink(), $msql, $user->getSessionsLink(), $module['id']);

// Check if this is the first time the user has loaded this module (in this session):
if($muser->checkSetup()){
$user->resetCache();
$timezone = $user->getTimezone();
}

include($root . $module['location'] . '/lang/' . $muser->getLanguage());
include($root . $module['location'] . 'message.php');

$message = new Message($module['id'], $module['name'], $themePath, $muser->getAdminEmail());

if((isset($_GET['p'])) && (is_numeric($_GET['p']))){
	$pageId = (int)$_GET['p'];

	switch($pageId){
		case 2: include( $root . $module['location'] . '/login.php'); break;
		case 3: include( $root . $module['location'] . '/logout.php'); break;
		case 4: include( $root . $module['location'] . '/join.php'); break;
		default: include($root . $module['location'] . '/home.php'); break;
	}

}else{
	include($root . $module['location'] . '/home.php');
}


?>
