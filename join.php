<?php
///////////////////////////////
//
//  join.php
//  New users join/register using this
//  page when enabled in the admin
//  panel. Included by module.php
//
//////////////////////////////

$registrationSetting = $msql->getSetting('joinSetting');

if(($registrationSetting['number'] == 1) || ($registrationSetting['number'] == 2)){ // IF user registration enabled; 1 = no verification; 2 = email verification
	if($user->getNumber()!=-1){ // IF user is already logged in
		include($root . $module['location'] . $themePath . 'header.html');
	        $message->showMessage(2);
	        include($root . $module['location'] . $themePath . 'footer.html');
	}else{ // ELSE IF user is already logged in
		if((isset($_POST['submit'])) && ($_POST['token'] == $user->getToken('UserSpace' . $module['id'] . 'join'))){ // IF post data was sent

			$registerErrors = [];
		        $userName = $sql->db_safe($_POST['userName']);
		        $password = $sql->db_safe($_POST['password']);
			$passwordVerify = $sql->db_safe($_POST['passwordVerify']);
			$email = $sql->db_safe($_POST['emailAddress']);

			if((strlen($userName)<3) || (strlen($userName)>25)){ array_push($registerErrors, mLang::join_userNameLength); }
	                if((strlen($password)<8) || (strlen($password)>70)){ array_push($registerErrors, mLang::join_passwordLength); }
	                if(strcmp($password, $passwordVerify) !== 0){ array_push($registerErrors, mLang::join_passwordMatch); }
	                if(strlen($email)<3){ array_push($registerErrors, mLang::join_emailValid); }

			// Check if username or email is already in use:
			if($sql->getUserByUsername($userName)>=0){ array_push($registerErrors, mLang::join_userNameInUse); }
			if($sql->getUserByEmail($email)>=0){ array_push($registerErrors, mLang::join_emailInUse); }

			if(count($registerErrors)>0){ // IF there is an error
				unset($password); unset($passwordVerify);
				include($root . $module['location'] . $themePath . 'header.html');
	                        include($root . $module['location'] . $themePath . 'join.html');
	                        include($root . $module['location'] . $themePath . 'footer.html');
			}else{ // ELSE IF there is an error

				$user->killToken('UserSpace' . $module['id'] . 'join');
				date_default_timezone_set('UTC'); // UTC is the database's default timezone. Convert it on echo/print.
				$joinDate = date("Y-m-d H:i:s");
		            	$uid = hash('sha256', mt_rand(0,100000) . rand(1,1000) . $joinDate . $userName);
				$ip = $user->getIP();

				if($registrationSetting['number'] == 2){
					// IF users need to verify their email address
					$activationCode = hash('sha256', mt_rand(0,100000) . rand(1,1000) . $joinDate . $userName);

					$subject = mLang::join_emailSubject;
			                $body = $message->generateEmailBody();
			                $body .= '<a href="' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . '?aac=' . $activationCode . '" target="_blank">' .$_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] . '?aac=' . $activationCode . '</a>';
			                $headers = 'MIME-Version: 1.0' . "\r\n";
			                $headers .= 'Content-type:text/html;charset=UTF-8' . "\r\n";
			                $issent = mail($email, $subject, $body, $headers);
					if(!$issent){ die(lang::register_emailCodeError); }
				}else{
					$activationCode = null;
				}

				// Insert user into database, addUser hashes the password for us:
				$userNumber = $sql->addUser($userName, $password, $email, $uid, $activationCode, 0, $module['id']);

				unset($password); unset($passwordVerify); unset($uid); unset($ip);


				if((isset($userNumber)) && (is_numeric($userNumber)) && ($userNumber > -1)){ // IF registration worked, ie. returned > -1, the user's number

					include($root . $module['location'] . $themePath . 'header.html');
		                        $message->showMessage(8); // User Joined/Registered Successfully
		                        include($root . $module['location'] . $themePath . 'footer.html');

				}else{ // ELSE IF registration worked, ie. returned > -1, the user's number
					include($root . $module['location'] . $themePath . 'header.html');
                                        $message->showMessage(10); // Error adding user
                                        include($root . $module['location'] . $themePath . 'footer.html');
				} // END ELSE IF registration worked, ie. returned > -1, the user's number

			} //END ELSE IF there is an error
		}else{ // ELSE IF post data was sent


			// Its important that the token setter isn't in the submit! That way the token isn't valid for attacks.
	                if(!$user->checkToken('UserSpace' . $module['id'] . 'join')){
	               		$user->setToken('UserSpace' . $module['id'] . 'join');
	                }

			include($root . $module['location'] . $themePath . 'header.html');
	                include($root . $module['location'] . $themePath . 'join.html');
	                include($root . $module['location'] . $themePath . 'footer.html');
		} // END ELSE IF post data was sent
	} // END ELSE IF user is already logged in
}else{ // ELSE IF user registration enabled
	include($root . $module['location'] . $themePath . 'header.html');
     	$message->showMessage(9); // Joining Disabled Message
    	include($root . $module['location'] . $themePath . 'footer.html');
} // END ELSE IF user registration enabled
?>

