<?php
/////////////////////////////////////////////////////////////////
//
//  mySQLiClass.php
//  Handles module specific queries to the database.
//
/////////////////////////////////////////////////////////////////

// In the interest of time, not all of these have been updated in object oriented format!

if(isset($includeCheck)){
// IF included from another source

class mSqlSwap{

	private $dbLink, $modulePostfix, $moduleId, $moduleLocation;
        protected $root;

        public function __construct($dbLink, $root, $moduleId, $moduleLocation, $modulePostfix){
                $includeCheck = 1;
                $this->root = $root;
                $this->dbLink = $dbLink;
		$this->moduleId = $moduleId;
		$this->moduleLocation = $moduleLocation;
		$this->modulePostfix = $modulePostfix;
        }

	public function getUserData($userId){
		// Like the one in SIGN, but specific to this module

                if($userId != (int)$userId){ die(lang::genericError); }

                if( $stmt = $this->dbLink->prepare('SELECT userId,userCanRead,userCanPost,userLanguage,pageId FROM UserSpace_' . $this->moduleId . '_Users_' . $this->modulePostfix . ' WHERE userId=? LIMIT 1')){
                        $stmt->bind_param('i', $userId);

                        if ($stmt->execute()){
                                $stmt->store_result();
                                $stmt->bind_result($result_userId, $result_userCanRead, $result_userCanPost, $result_userLanguage, $result_pageId);

                                while ($stmt->fetch()){
                                        $userData = array('userId' => $result_userId, 'userCanRead' => $result_userCanRead, 'userCanPost' => $result_userCanPost, 'userLanguage' => $result_userLanguage, 'pageId' => $result_pageId);
                                }
                        }else{
                            die(lang::genericError);
                        }
                }else{
                        die(lang::genericError);
                }

                $stmt->close();
                unset($stmt);

                return $userData;
        }

	public function getSetting($name){
                // Returns the values of the module setting with the given name, or null on error

                if( $stmt = $this->dbLink->prepare('SELECT settingNumber, settingString FROM UserSpace_' . $this->moduleId . '_Settings_' . $this->modulePostfix . ' WHERE settingName=? LIMIT 1')){
                        $stmt->bind_param('s', $name);

                        if($stmt->execute()){
                                $setting = [];
                                $stmt->store_result();
                                $stmt->bind_result($setting['number'], $setting['string']);
                                $stmt->fetch();

                                if($stmt->num_rows != 1){
                                        return null;
                                }
                        }else{
                                return null;
                        }
                }else{
                        return null;
                }

                $stmt->close();
                unset($stmt);

                return $setting;
        }


} // END mSqlSwap Class

}else{ // ELSE IF included from another php source
if(headers_sent()){ die(include('../404.html')); } // If headers were somehow already sent, just die to a custom 404 page
header('HTTP/1.1 404 Not Found'); // Send headers if not sent already
die(include('../404.html')); // Die to custom 404 page
} // END ELSE IF included from another source
?>
