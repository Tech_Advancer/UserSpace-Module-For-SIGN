<?php
//////////////////////////////
//
// login.php
//
//////////////////////////////

$loginErrors = []; // initialize an errors array

if($user->getNumber() >= 0){ // IF we are somehow already logged in
	include($root . $module['location'] . $themePath . 'header.html');
	$message->showMessage(2);
	include($root . $module['location'] . $themePath . 'footer.html');

}else{ // ELSE IF we are somehow already logged in
	if((isset($_POST['submit'])) && (isset($_POST['username'])) && (isset($_POST['password']))){ // IF we are logging in
		if($_POST['token'] == $user->getToken('login')){ // IF token is good

			$username = $_POST['username'];
		        $password = $_POST['password'];
		        if(isset($_POST['stay'])){ $stay=true; }else{ $stay=false; }

			$login = $user->login($username, $password, $stay); // $user->login returns an array and sets up session data. ['login'] = true/false; ['status'] = error messages or user id

			if($login['login']){
				$user->killToken('login');

				if($user->getAccountActivation()){ // IF the account is activated
					if(!$user->getBan((int)$user->getNumber())){ // IF the user isn't banned
						$user->recordIP();

						$user->resetCache();
						$muser->resetCache();

						$timezone = $user->getTimezone();
						$language = $muser->getLanguage();

						include($root . $module['location'] . $themePath . 'header.html');
					        $message->showMessage(4); // good login message
					        include($root . $module['location'] . $themePath . 'footer.html');
					}else{ // ELSE IF the user isn't banned
						$banInfo = $user->getBanInfo($user->getNumber());
						$user->logout();
						include($root . $module['location'] . $themePath . 'header.html');
                                                $message->showBan($banInfo['banDate'], $sql->xssafe($banInfo['banReason'])); // ban message
						$user->logout();
                                                include($root . $module['location'] . $themePath . 'footer.html');
					} // END ELSE IF the user isn't banned
				}else{ // ELSE IF the account is activated
					//account needs activation
					$user->logout();
					include($root . $module['location'] . $themePath . 'header.html');
                                        $message->showMessage(7); // account activation message
                                        include($root . $module['location'] . $themePath . 'footer.html');
				} // END ELSE IF the account is activated
			}else{
				if($login['status'] == -1){ // Bad Login
					include($root . $module['location'] . $themePath . 'header.html');
					array_push($loginErrors, mLang::login_bad);
		                        include($root . $module['location'] . $themePath . 'login.html');
		                        include($root . $module['location'] . $themePath . 'footer.html');
				}

				if($login['status'] == -2){ // Max Login Attempts
					include($root . $module['location'] . $themePath . 'header.html');
					$timer = $sql->getSetting('loginLockTime');
					$message->showLoginLockTime($timer['number']);
                                        include($root . $module['location'] . $themePath . 'footer.html');
				}
			}
		}else{ // ELSE IF token is good
                	include($root . $module['location'] . $themePath . 'header.html');
	                $message->showMessage(1);
	                include($root . $module['location'] . $themePath . 'footer.html');
	        } // END ELSE IF token is good

	}else{ // ELSE IF we are logging in (aka POST data NOT sent):

			// Its important that the token setter isn't in the submit! That way the token isn't valid for attacks.
			if(!$user->checkToken('login')){
				$user->setToken('login');
			}

			include($root . $module['location'] . $themePath . 'header.html');
			include($root . $module['location'] . $themePath . 'login.html');
			include($root . $module['location'] . $themePath . 'footer.html');
	} // END ELSE IF we are logging in
} //END ELSE IF we are somehow already logged in

?>
