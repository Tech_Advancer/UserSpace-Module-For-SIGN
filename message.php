<?php

class Message{

	private $message = '', $moduleId, $themePath, $moduleName, $adminEmail;

	public function __construct($moduleId, $moduleName, $themePath, $adminEmail){
		$this->moduleId = (int)$moduleId;
		$this->themePath = $themePath;
		$this->moduleName = $moduleName;
		$this->adminEmail = $adminEmail;
	}

	public function showMessage($messageNum){

		if(is_numeric($messageNum)){

			switch((int)$messageNum){
				case 1: $message = lang::genericError; break;
				case 2: $message = mLang::login_alreadyLoggedIn1 . $this->moduleId . mLang::login_alreadyLoggedIn2; break;
				case 3: $message = ''; break;
				case 4: $message = mLang::login_good1 . $this->moduleId . mLang::login_good2; break;
				case 5: $message = mLang::logout1 . $this->moduleId . mLang::logout2; break;
				case 6:
					$banInfo = $banInfo = $this->user->getBanInfo($this->user->getNumber());
					$banDate = date_create($banInfo['banDate']);
					$message = mLang::ban1 . date_format($banDate,"F d, Y") . mLang::ban2 . $banInfo['banReason']; break;
				case 7: $message = mLang::activation; break;
				case 8: $message = mLang::join_success; break;
				case 9: $message = mLang::join_disabled1 . $this->moduleId . mLang::join_disabled2; break;
				case 10: $message = mLang::join_errorAddingUser1 . $this->adminEmail . mLang::join_errorAddingUser2 . $this->adminEmail . join_errorAddingUser3;
				default: $message = mLang::invalidMessage; break;
			}

			include($this->themePath . 'message.html');

		}else{
			$message = mLang::invalidMessage;
			include($this->themePath . 'message.html');
		}

	}

	public function showLoginLockTime($timer){
        	$message = mLang::login_maxAttempts1 . (int)$timer . mLang::login_maxAttempts2;
		include($this->themePath . 'message.html');
	}

	public function showBan($banDate, $banReason){
                $theDate = date_create($banDate);
               	$message = mLang::ban1 . date_format($theDate,"F d, Y") . mLang::ban2 . $banReason;
		include($this->themePath . 'message.html');
	}

	public function generateEmailBody(){
		$message = mLang::join_emailBody1 .$this->moduleName . mLang::join_emailBody2;
		return $message;
	}

}

?>
